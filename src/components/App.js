import React from 'react'
import {Route} from 'react-router-dom'

import Layout from './UI/Layout/Layout'
import {withStyles} from '@material-ui/core/styles';

import ProjectsList from './Projects/ProjectsList/ProjectsList'

const styles = {
    root: {
        flexGrow: 1,
    },
    appFrame: {
        height: 430,
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
}

const app = props => {
    const {classes} = props

    return (
        <div className={classes.root}>
            <div className={classes.appFrame}>
                <Layout>
                    <Route path="/" component={ProjectsList}/>
                </Layout>
            </div>
        </div>
    );
}

export default withStyles(styles)(app)
