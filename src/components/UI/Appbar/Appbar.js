import React from 'react'

import classNames from 'classnames';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import logo from '../../../logo.svg';

const drawerWidth = 240;

const styles = theme => ({
    appBar: {
        backgroundColor: '#222',
        'display': 'flex',
        'flexDirection': 'row',
        'justifyContent': 'flex-start',
        position: 'absolute',
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    'appBarShift-left': {
        marginLeft: drawerWidth,
    },
    'appBarShift-right': {
        marginRight: drawerWidth,
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 20,
    },
    hide: {
        display: 'none',
    },
    'content-left': {
        marginLeft: -drawerWidth,
    },
    'content-right': {
        marginRight: -drawerWidth,
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    'contentShift-left': {
        marginLeft: 0,
    },
    'contentShift-right': {
        marginRight: 0,
    },
    logo: {
        'animation': 'App-logo-spin infinite 20s linear',
        'height': '30px'
    },
    title: {
        'fontSize': '1.5em',
        'color': theme.palette.primary.main,
        backgroundColor: '#222'
    },
    appBarTitle: {
        display: 'flex',
        flexDirection: 'row',
        justifyContenmt: 'flex-start',
    }
});

const appbar = props => {
    const {classes, anchor, open} = props

    return (
        <AppBar
            className={classNames(classes.appBar, {
                [classes.appBarShift]: open,
                [classes[`appBarShift-${anchor}`]]: open,
            })}
        >
            <Toolbar disableGutters={!open}>
                <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    onClick={() => props.openDrawer()}
                    className={classNames(classes.menuButton, open && classes.hide)}
                >
                    <MenuIcon/>
                </IconButton>
                <div className={classes.appBarTitle}>
                    <img className={classes.logo} src={logo} alt="logo"/>
                    <Typography className={classes.title}>
                        React - Redux Example
                    </Typography>
                </div>
            </Toolbar>
        </AppBar>
    )
}

export default withStyles(styles)(appbar)