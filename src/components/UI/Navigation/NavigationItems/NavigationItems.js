import React from 'react';

// material ui components
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

// icons
import ProjectsIcon from '@material-ui/icons/Storage';
import AddProjectsIcon from '@material-ui/icons/AddBox';
import RemoveProjectsIcon from '@material-ui/icons/IndeterminateCheckBox';

export const navigationItems = (
    <div>
        <ListItem button>
            <ListItemIcon>
                <ProjectsIcon />
            </ListItemIcon>
            <ListItemText primary="Projects" />
        </ListItem>
        <ListItem button>
            <ListItemIcon>
                <AddProjectsIcon />
            </ListItemIcon>
            <ListItemText primary="add Project" />
        </ListItem>
        <ListItem button>
            <ListItemIcon>
                <RemoveProjectsIcon />
            </ListItemIcon>
            <ListItemText primary="Send mail" />
        </ListItem>
    </div>
);
