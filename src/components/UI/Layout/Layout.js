import React,{Component} from 'react'
import classNames from 'classnames';

import {withStyles} from '@material-ui/core/styles';
import Aux from '../../../hoc/Aux/Aux'
import Appbar from '../Appbar/Appbar'
import Navigation from '../Navigation/Navigation'

const drawerWidth = 240;

const styles = theme => ({
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing.unit * 3,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    'content-left': {
        marginLeft: -drawerWidth,
    },
    'content-right': {
        marginRight: -drawerWidth,
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    'contentShift-left': {
        marginLeft: 0,
    },
    'contentShift-right': {
        marginRight: 0,
    }
});

class Layout extends Component {
    state = {
        open: false,
        anchor: 'left',
    }

    handleDrawerOpen = () => {
        this.setState({open: true});
    };

    handleDrawerClose = () => {
        this.setState({open: false});
    };

    render() {
        const {classes} = this.props
        const {anchor, open} = this.state

        return(
            <Aux>
                <Appbar anchor={anchor} open={open} openDrawer={this.handleDrawerOpen}/>
                <Navigation anchor={anchor} open={open} closeDrawer={this.handleDrawerClose}/>
                <main
                className={classNames(classes.content, classes[`content-${anchor}`], {
                [classes.contentShift]: open,
                [classes[`contentShift-${anchor}`]]: open,
                })}
                >
                <div className={classes.drawerHeader} />
                    {this.props.children}
                </main>
            </Aux>
        )
    }
}

export default withStyles(styles)(Layout)