import React from 'react'
import {connect} from 'react-redux'

const projectsList = props => (
    <ul>
        {props.projects.map(project => <li key={project.id}>
            <div>Firma: {project.company}</div>
            <div>Project: {project.name}</div>
            <div>
                Technologies:
                <ul>
                    {project.technologies.map((technology, i) => <li key={i}>{technology}</li>)}
                </ul>
            </div>
            <div>
                Developers:
                <ul>
                    {project.developers.map((developer, i) => <li key={i}>{developer}</li>)}
                </ul>
            </div>
        </li>)}
    </ul>
)

function mapStateToProps(state) {
    return {
        projects: state.projectReducer
    }
}

export default connect(mapStateToProps)(projectsList)