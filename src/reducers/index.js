// reducers
import projectReducer from './projectReducer'

// redux packages
import { combineReducers } from 'redux'

// create rootReducer
const rootReducer = combineReducers({
    projectReducer
})

export default rootReducer
