import {createMuiTheme} from '@material-ui/core/styles';

const theme = createMuiTheme({
    breakpoints: {
        values: {
            xs: 0,
            sm: 600,
            md: 1000,
            lg: 1200,
            xl: 1920
        }
    },
    /*color object*/
    palette: {
        primary: {
            main: '#43d9f8',
            contrastText: '#fff'
        },
        secondary: {
            main: '#d4d5d5',
            dark: '#e8e8e8',
            contrastText: '#f67d2c'
        },
        error: {
            main: '#B92D00',
        },
        custom: {
            darkGrey: '#707070',
        }
    },
    /*typography object*/
    // typography: {
    //     fontFamily: [
    //         '"Montserrat"',
    //         'sans-serif'
    //     ].join(','),
    //     fontSize: 14,
    //     button: {
    //         fontWeight: '500',
    //     },
    //     headline: {
    //         color: '#f67d2c',
    //         font: 16
    //     },
    //     paragraphHeadline: {
    //         color: '#707070',
    //         font: 16
    //     },
    //     paragraph: {
    //         color: '#707070',
    //         font: 14
    //     },
    //     paragraphSmall: {
    //         color: '#707070',
    //         font: 10
    //     }
    // },
})

export default theme