// react packages
import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';

// import { BrowserRouter as Router, Route, Link } from "react-router-dom"
import {BrowserRouter} from 'react-router-dom'


// material ui components / packages
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider'
import mauiTheme from './maui-theme'

// styling
import './index.css';
import './assets/header.css';
// component
import App from './components/App'

// store and redux implementation
import { createStore } from 'redux'
import { Provider } from 'react-redux'  // provides store access for all components
import rootReducer from './reducers/index'

const store = createStore(
    rootReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

ReactDOM.render(
    <MuiThemeProvider theme={mauiTheme}>
        <Provider store={store}>
            <BrowserRouter>
                <App />
            </BrowserRouter>
        </Provider>
    </MuiThemeProvider>,
    document.getElementById('root')
);

registerServiceWorker();
